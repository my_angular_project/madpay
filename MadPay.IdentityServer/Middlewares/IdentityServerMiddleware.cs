﻿using IdentityServer4.Models;
using MadPay.Domain.Entities.Security;
using MadPay.IdentityServer;
using MadPay.Persistence.Context;
using MadPay.Services.Service.Security.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace MadPay.Api.Middlewares
{
    public static class IdentityServerMiddleware
    {
        public static void AddIdentityServerConfig(this IServiceCollection services)
        {
            //services.AddIdentity<User, Role>(opt =>
            //{
            //    opt.Password.RequireLowercase = false;
            //    opt.Password.RequireUppercase = false;
            //    opt.Password.RequireNonAlphanumeric = false;

            //    opt.User.RequireUniqueEmail = true;

            //    opt.SignIn.RequireConfirmedAccount = true;
            //    opt.SignIn.RequireConfirmedEmail = true;
            //})
            //.AddEntityFrameworkStores<ApplicationDBContext>()
            //.AddUserManager<AppUserManager>()
            //.AddErrorDescriber<AppErrorDescriberService>()
            //.AddClaimsPrincipalFactory<AppUserClaimsPrincipalService>()
            //.AddDefaultTokenProviders();

            services.AddIdentityServer()
                    .AddAspNetIdentity<User>()
                    .AddInMemoryIdentityResources(Config.GetIdentityResources())
                    .AddInMemoryApiResources(Config.GetApiResource())
                    .AddInMemoryClients(Config.GetClients())
                    .AddDeveloperSigningCredential();
        }
    }
}
