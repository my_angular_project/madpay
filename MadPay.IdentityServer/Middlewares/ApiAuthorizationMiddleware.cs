﻿using Microsoft.Extensions.DependencyInjection;

namespace MadPay.Api.Middlewares
{
    /// <summary>
    /// For Config Authentication
    /// </summary>
    public static class ApiAuthorizationMiddleware
    {
        public static void AddApiAuthorization(this IServiceCollection services)
        {
            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", opt =>
                 {
                     opt.Audience = "MadPay";
                     opt.RequireHttpsMetadata = true;
                     opt.Authority = "https://localhost:6500";
                 });
        }
    }
}
