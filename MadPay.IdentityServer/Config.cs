﻿using IdentityServer4.Models;
using System.Collections.Generic;

namespace MadPay.IdentityServer
{
    public class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources() =>
          new List<IdentityResource>
          {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
          };

        public static IEnumerable<ApiResource> GetApiResource() =>
          new List<ApiResource>
          {
                new ApiResource("MadPay","MadPay Resource")
          };

        public static IEnumerable<Client> GetClients() =>
            new List<Client> {
                new Client {
                    ClientId = "angular",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    RequirePkce = true,
                    RequireClientSecret = false,
                    RequireConsent = false,
                    //RedirectUris = { "http://localhost:4200" },
                    //PostLogoutRedirectUris = { "http://localhost:4200" },
                    AllowedCorsOrigins = { "http://localhost:4200" },

                    AllowedScopes = { "MadPay" },
                    AllowAccessTokensViaBrowser = true
                }
            };
    }
}
