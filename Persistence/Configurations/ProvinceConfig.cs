﻿using MadPay.Domain.Entities.Countrya;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.Country
{
    public class ProvinceConfig : IBaseEntityConfig<Province>
    {
        public void Configure(EntityTypeBuilder<Province> builder)
        {
            //Fields
            builder.HasKey(k => k.Id);

            builder.HasOne(i => i.Country).WithMany(p => p.Provinces).HasForeignKey(i => i.CountryId);

            //Table
            builder.ToTable("Province", "Geo");
        }
    }
}
