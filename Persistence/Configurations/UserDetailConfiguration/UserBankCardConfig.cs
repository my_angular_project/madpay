﻿using MadPay.Domain.Entities.UserDetail;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.UserDetailConfiguration
{
    public class UserBankCardConfig : IBaseEntityConfig<UserBankCard>
    {
        public void Configure(EntityTypeBuilder<UserBankCard> builder)
        {
            //Fields
            builder.HasKey(k => k.Id);
            builder.Property(i => i.BankName).HasMaxLength(50).IsRequired();
            builder.Property(i => i.Shaba).HasColumnType("VARCHAR(50)").IsRequired();
            builder.Property(i => i.CardNumber).HasColumnType("VARCHAR(16)").IsRequired();
            builder.Property(i => i.ExpireDateMonth).HasColumnType("VARCHAR(2)").IsRequired();
            builder.Property(i => i.ExpireDateYear).HasColumnType("VARCHAR(2)").IsRequired();
            builder.Property(i => i.IsActive).IsRequired();

            //Navigations
            builder.HasOne(i => i.User).WithMany(p => p.UserBankCards).HasForeignKey(i => i.UserId).IsRequired();

            //Table
            builder.ToTable("UserBankCard", SchemaConfig.UserDetail);
        }
    }
}
