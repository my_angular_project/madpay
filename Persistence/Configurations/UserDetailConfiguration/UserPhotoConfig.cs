﻿using MadPay.Domain.Entities.UserDetail;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.UserDetailConfiguration
{
    public class UserPhotoConfig : IBaseEntityConfig<UserPhoto>
    {
        public void Configure(EntityTypeBuilder<UserPhoto> builder)
        {
            //Fields
            builder.HasKey(k => k.Id);
            builder.Property(i => i.Url).HasMaxLength(300).IsRequired();
            builder.Property(i => i.Title).HasMaxLength(200).IsRequired();
            builder.Property(i => i.IsMain).IsRequired();
            builder.Property(i => i.IsActive).IsRequired();

            //Navigations
            builder.HasOne(i => i.User).WithMany(p => p.UserPhotos).HasForeignKey(i => i.UserId).IsRequired();

            //Table
            builder.ToTable("UserPhoto", SchemaConfig.UserDetail);
        }
    }
}
