﻿using MadPay.Domain.Entities.Security;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.SecurityConfiguration
{
    public class PermissionConfig : IBaseEntityConfig<Permission>
    {
        public void Configure(EntityTypeBuilder<Permission> builder)
        {
            //Fields
            builder.HasKey(k => k.Id);
            builder.Property(i => i.Name).HasColumnType("NVARCHAR(150)").IsRequired();
            builder.Property(i => i.Controller).HasColumnType("VARCHAR(150)").IsRequired();
            builder.Property(i => i.Action).HasColumnType("VARCHAR(100)").IsRequired();
            builder.Property(i => i.ActionType).HasColumnType("TINYINT").IsRequired();

            //Relation
            builder.HasOne(i => i.PermissionGroup)
                   .WithMany(p => p.Permissions)
                   .HasForeignKey(i => i.PermissionGroupID)
                   .IsRequired();

            //Table
            builder.ToTable("Permission", SchemaConfig.Security);
        }
    }
}
