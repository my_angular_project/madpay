﻿using MadPay.Domain.Entities.Security;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.SecurityConfiguration
{
    public class RolePermissionConfig : IBaseEntityConfig<RolePermission>
    {
        public void Configure(EntityTypeBuilder<RolePermission> builder)
        {
            //Fields
            builder.HasKey(i => new { i.PermissionID, i.RoleID });

            //Relation
            builder.HasOne(i => i.Permission)
                   .WithMany(p => p.RolePermissions)
                   .HasForeignKey(i => i.PermissionID)
                   .IsRequired();

            builder.HasOne(i => i.Role)
                   .WithMany(p => p.RolePermissions)
                   .HasForeignKey(i => i.RoleID)
                   .IsRequired();

            //Table
            builder.ToTable("RolePermission", SchemaConfig.Security);
        }
    }
}
