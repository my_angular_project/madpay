﻿using MadPay.Domain.Entities.Security;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.SecurityConfiguration
{
    public class RoleConfig : IBaseEntityConfig<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            //Fields
            builder.HasKey(k => k.Id);
            builder.Property(i => i.Name).HasColumnType("NVARCHAR(150)").IsRequired();
            builder.HasIndex(i => i.Name).IsUnique();
            builder.Property(i => i.NormalizedName).HasColumnType("NVARCHAR(150)");

            //Relation
            builder.HasOne(i => i.Registerer)
                   .WithMany(p => p.RoleRegisterer)
                   .HasForeignKey(i => i.RegistererID)
                   .IsRequired();

            builder.HasOne(i => i.Modifier)
               .WithMany(p => p.RoleModifier)
               .HasForeignKey(i => i.ModifierID);

            //Table
            builder.ToTable("Role", SchemaConfig.Security);
        }
    }
}
