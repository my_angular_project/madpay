﻿using MadPay.Domain.Entities.Security;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.SecurityConfiguration
{
    public class UserRoleConfig : IBaseEntityConfig<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            //Fields
            builder.HasKey(r => new { r.UserId, r.RoleId });

            //Relation
            builder.HasOne(i => i.User)
                   .WithMany(p => p.UserRoles)
                   .HasForeignKey(i => i.UserId)
                   .IsRequired()
                   .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(i => i.Role)
                   .WithMany(p => p.UserRoles)
                   .HasForeignKey(i => i.RoleId)
                   .IsRequired()
                   .OnDelete(DeleteBehavior.NoAction);

            //Table
            builder.ToTable("UserRole", SchemaConfig.Security);
        }
    }
}
