﻿using MadPay.Domain.Entities.Security;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.SecurityConfiguration
{
    public class UserTokenConfig : IBaseEntityConfig<UserToken>
    {
        public void Configure(EntityTypeBuilder<UserToken> builder)
        {
            //Fields
            builder.Property(i => i.Value).HasColumnType("VARCHAR(200)");

            //Table
            builder.ToTable("UserToken", SchemaConfig.Security);
        }
    }
}
