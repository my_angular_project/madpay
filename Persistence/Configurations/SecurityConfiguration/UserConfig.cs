﻿using MadPay.Domain.Entities.Security;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.SecurityConfiguration
{
    public class UserConfig : IBaseEntityConfig<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            //Fields
            builder.HasKey(k => k.Id);
            builder.Property(i => i.UserName).HasColumnType("VARCHAR(100)").IsRequired();
            builder.HasIndex(i => i.UserName).IsUnique();
            builder.Property(i => i.NormalizedUserName).HasColumnType("VARCHAR(100)").IsRequired();
            builder.Property(i => i.Email).HasColumnType("VARCHAR(100)").IsRequired();
            builder.HasIndex(i => i.Email).IsUnique();
            builder.Property(i => i.NormalizedEmail).HasColumnType("VARCHAR(100)").IsRequired();
            builder.Property(i => i.EmailConfirmed).IsRequired();
            builder.Property(i => i.PhoneNumber).HasColumnType("VARCHAR(11)").IsRequired();
            builder.Property(i => i.FirstName).HasMaxLength(100).IsRequired();
            builder.Property(i => i.LastName).HasMaxLength(100).IsRequired();
            builder.Property(i => i.PasswordHash).HasColumnType("VARCHAR(200)").IsRequired();
            builder.Property(i => i.SecurityStamp).HasColumnType("VARCHAR(100)").IsRequired();
            builder.Property(i => i.ConcurrencyStamp).HasColumnType("VARCHAR(100)").IsRequired();
            builder.Property(i => i.AccessFailedCount).HasColumnType("TINYINT");
            builder.Property(i => i.City).HasMaxLength(100);
            builder.Property(i => i.BirthDate).HasColumnType("DATE");

            //Table
            builder.ToTable("User", SchemaConfig.Security);
        }
    }
}
