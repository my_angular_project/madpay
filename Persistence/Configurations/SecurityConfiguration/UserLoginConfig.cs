﻿using MadPay.Domain.Entities.Security;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.SecurityConfiguration
{
    public class UserLoginConfig : IBaseEntityConfig<UserLogin>
    {
        public void Configure(EntityTypeBuilder<UserLogin> builder)
        {
            //Fields
            builder.HasKey(i => new { i.LoginProvider, i.ProviderKey, i.UserId });
            builder.Property(i => i.ProviderDisplayName).HasColumnType("VARCHAR(200)");

            //Table
            builder.ToTable("UserLogin", SchemaConfig.Security);
        }
    }
}
