﻿using MadPay.Domain.Entities.Security;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.SecurityConfiguration
{
    public class PermissionGroupConfig : IBaseEntityConfig<PermissionGroup>
    {
        public void Configure(EntityTypeBuilder<PermissionGroup> builder)
        {
            //Fields
            builder.HasKey(k => k.Id);
            builder.Property(i => i.Name).HasColumnType("NVARCHAR(150)").IsRequired();
            builder.Property(i => i.Controller).HasColumnType("VARCHAR(150)").IsRequired();

            //Relation
            builder.HasOne(i => i.ParentPermissionGroup)
                   .WithMany(p => p.PermissionGroups)
                   .HasForeignKey(i => i.ParentID);

            //Table
            builder.ToTable("PermissionGroup", SchemaConfig.Security);
        }
    }
}
