﻿using MadPay.Domain.Entities.Security;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.SecurityConfiguration
{
    public class UserClaimConfig : IBaseEntityConfig<UserClaim>
    {
        public void Configure(EntityTypeBuilder<UserClaim> builder)
        {
            //Fields
            builder.HasKey(i => i.Id);
            builder.Property(i => i.ClaimType).HasColumnType("VARCHAR(100)").IsRequired();
            builder.Property(i => i.ClaimValue).HasColumnType("VARCHAR(100)").IsRequired();

            //Table
            builder.ToTable("UserClaim", SchemaConfig.Security);
        }
    }
}
