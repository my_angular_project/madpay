﻿namespace MadPay.Persistence.Configurations.BaseConfig
{
    public static class SchemaConfig
    {
        public static string Security => "Security";
        public static string UserDetail => "UserDetail";
    }
}
