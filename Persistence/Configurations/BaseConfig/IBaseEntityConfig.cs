﻿using Microsoft.EntityFrameworkCore;

namespace MadPay.Persistence.Configurations
{
    public interface IBaseEntityConfig<T> : IEntityTypeConfiguration<T> where T : class { }
}
