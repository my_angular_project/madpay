﻿using MadPay.Domain.Entities;
using MadPay.Persistence.Configurations;
using MadPay.Persistence.Context.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using MadPay.Domain.Entities.Security;

namespace MadPay.Persistence.Context
{
    public class ApplicationDBContext : IdentityDbContext<User, Role, long, UserClaim, UserRole, UserLogin, RoleClaim, UserToken>
    {
        #region == Ctor ==
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options) { }

        public ApplicationDBContext() : base() { }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.AddDBSetFromModel(typeof(IEntity).Assembly, typeof(IEntity));
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(IBaseEntityConfig<>).Assembly);
        }
    }
}
