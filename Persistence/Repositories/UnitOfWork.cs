﻿using AutoMapper;
using MadPay.Domain.Entities.Geo;
using MadPay.Domain.Entities.Security;
using MadPay.Services.Interface.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace MadPay.Persistence.Repositories
{
    public class UnitOfWork<TContext> : IUnitOfWork where TContext : DbContext, new()
    {
        #region == Ctor ==
        private TContext _context;
        //private readonly IDbFactory<TContext> _dbFactory;
        private bool _disposed = false;
        private IMapper _mapper;


        public UnitOfWork(TContext context, IMapper mapper)
        {
            this._context = context;
            //this._dbFactory = dbFactory;
            this._mapper = mapper;
        }

        //public TContext DbContext
        //{
        //    get { return _context ?? (_context = _dbFactory.Init()); }
        //}

        #endregion

        #region == Init Repository ==
        //TODO Delete Sample Repository
        private IBaseRepository<Countries> _countryRepository;

        private IBaseRepository<User> _userRepository;
        private IBaseRepository<Role> _roleRepository;
        private IBaseRepository<Permission> _permissionRepository;
        private IBaseRepository<PermissionGroup> _permissionGroupRepository;
        #endregion

        #region == Declare Repository ==
        //TODO Delete Sample Repository
        public IBaseRepository<Countries> CountryRepository => _countryRepository ?? (_countryRepository = new BaseRepository<Countries, TContext>(_context, _mapper));


        #region == Security ==
        public IBaseRepository<User> UserRepository => _userRepository ??= new BaseRepository<User, TContext>(_context, _mapper);
        public IBaseRepository<Role> RoleRepository => _roleRepository ??= new BaseRepository<Role, TContext>(_context, _mapper);
        public IBaseRepository<Permission> PermissionRepository => _permissionRepository ??= new BaseRepository<Permission, TContext>(_context, _mapper);
        public IBaseRepository<PermissionGroup> PermissionGroupRepository => _permissionGroupRepository ??= new BaseRepository<PermissionGroup, TContext>(_context, _mapper);
        #endregion

        #endregion

        #region == Base ==
        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
