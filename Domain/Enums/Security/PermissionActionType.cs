﻿namespace MadPay.Domain.Enums.Security
{
    /// <summary>
    /// انواع عملیات در دسترسی
    /// </summary>
    public enum PermissionActionType : byte
    {
        /// <summary>
        /// دریافت
        /// </summary>
        Read = 1,

        /// <summary>
        /// افزودن
        /// </summary>
        Insert = 2,

        /// <summary>
        /// ویرایش
        /// </summary>
        Update = 3,

        /// <summary>
        /// حذف
        /// </summary>
        Delete = 4
    }
}
