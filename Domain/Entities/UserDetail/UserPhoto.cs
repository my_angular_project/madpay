﻿using MadPay.Domain.Entities.Security;
using System;

namespace MadPay.Domain.Entities.UserDetail
{
    /// <summary>
    /// عکس کاربر
    /// </summary>
    public class UserPhoto : BaseEntity<int>, IRegister<long>, IActive
    {
        #region == Fields ==
        /// <summary>
        /// آدرس
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// عنوان
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// آیا عکس پروفایل است
        /// </summary>
        public bool IsMain { get; set; }

        /// <summary>
        /// کلید کاربر
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// وضعیت
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// ثبت کننده
        /// </summary>
        public long RegistererID { get; set; }

        /// <summary>
        /// تاریخ ثبت
        /// </summary>
        public DateTime RegisterDate { get; set; }
        #endregion

        #region == Navigation ==
        /// <summary>
        /// جدول کاربر
        /// </summary>
        public User User { get; set; }
        #endregion
    }
}
