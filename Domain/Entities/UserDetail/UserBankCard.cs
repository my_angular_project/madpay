﻿using MadPay.Domain.Entities.Security;
using System;

namespace MadPay.Domain.Entities.UserDetail
{
    /// <summary>
    /// کارت های بانکی کاربر
    /// </summary>
    public class UserBankCard : BaseEntity<int>, IRegister<long>, IModifier<long>, IActive
    {
        #region == Fields ==
        /// <summary>
        /// نام بانک
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// شماره شبا
        /// </summary>
        public string Shaba { get; set; }

        /// <summary>
        /// شماره کارت
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// تاریخ انقضا (ماه)
        /// </summary>
        public string ExpireDateMonth { get; set; }

        /// <summary>
        /// تاریخ انقضا (سال)
        /// </summary>
        public string ExpireDateYear { get; set; }

        /// <summary>
        /// کلید کاربر
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// وضعیت
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// ثبت کننده
        /// </summary>
        public long RegistererID { get; set; }

        /// <summary>
        /// تاریخ ثبت
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// ویرایش کننده
        /// </summary>
        public long? ModifierID { get; set; }

        /// <summary>
        /// تاریخ ویرایش
        /// </summary>
        public DateTime? ModifyDate { get; set; }
        #endregion

        #region == Navigation ==
        /// <summary>
        /// جدول کاربر
        /// </summary>
        public User User { get; set; }
        #endregion
    }
}
