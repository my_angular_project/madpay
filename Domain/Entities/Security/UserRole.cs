﻿using Microsoft.AspNetCore.Identity;

namespace MadPay.Domain.Entities.Security
{
    /// <summary>
    /// نقش های هر کاربر
    /// </summary>
    public class UserRole : IdentityUserRole<long>, IEntity
    {
        #region == Navigation ==
        public User User { get; set; }
        public Role Role { get; set; }
        #endregion
    }
}
