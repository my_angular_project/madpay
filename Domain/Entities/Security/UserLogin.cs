﻿using Microsoft.AspNetCore.Identity;

namespace MadPay.Domain.Entities.Security
{
    /// <summary>
    /// لاگین های هر کاربر
    /// </summary>
    public class UserLogin : IdentityUserLogin<long>, IEntity
    {
    }
}
