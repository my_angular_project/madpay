﻿namespace MadPay.Domain.Entities.Security
{
    /// <summary>
    /// دسترسی های نقش
    /// </summary>
    public class RolePermission : IEntity
    {
        #region == Field ==
        /// <summary>
        /// کلید دسترسی
        /// </summary>
        public int PermissionID { get; set; }

        /// <summary>
        /// کلید نقش
        /// </summary>
        public long RoleID { get; set; }
        #endregion

        #region == Navigation ==
        /// <summary>
        /// جدول دسترسی
        /// </summary>
        public Permission Permission { get; set; }

        /// <summary>
        /// جدول نقش
        /// </summary>
        public Role Role { get; set; }
        #endregion
    }
}
