﻿using Microsoft.AspNetCore.Identity;

namespace MadPay.Domain.Entities.Security
{
    /// <summary>
    /// کلیم های هر کاربر
    /// </summary>
    public class UserClaim : IdentityUserClaim<long>, IEntity
    {
    }
}
