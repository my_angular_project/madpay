﻿using System.Collections.Generic;

namespace MadPay.Domain.Entities.Security
{
    /// <summary>
    /// گروه بندی دسترسی
    /// </summary>
    public class PermissionGroup : BaseEntity<int>
    {
        #region == Field ==
        /// <summary>
        /// کلید پدر
        /// </summary>
        public int? ParentID { get; set; }

        /// <summary>
        /// نام
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// نام کنترلر
        /// </summary>
        public string Controller { get; set; }
        #endregion

        #region == Navigation ==
        /// <summary>
        /// جدول پدر گروه بندی دسترسی 
        /// </summary>
        public PermissionGroup ParentPermissionGroup { get; set; }

        /// <summary>
        /// جدول فرزندان گروه بندی دسترسی
        /// </summary>
        public ICollection<PermissionGroup> PermissionGroups { get; set; }

        /// <summary>
        /// جدول دسترسی ها
        /// </summary>
        public ICollection<Permission> Permissions { get; set; }
        #endregion
    }
}
