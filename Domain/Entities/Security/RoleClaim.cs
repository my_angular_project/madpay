﻿using Microsoft.AspNetCore.Identity;

namespace MadPay.Domain.Entities.Security
{
    /// <summary>
    /// کلیم های هر نقش
    /// </summary>
    public class RoleClaim : IdentityRoleClaim<long>, IEntity
    {
    }
}
