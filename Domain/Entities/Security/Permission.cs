﻿using MadPay.Domain.Enums.Security;
using System.Collections.Generic;

namespace MadPay.Domain.Entities.Security
{
    /// <summary>
    /// دسترسی
    /// </summary>
    public class Permission : BaseEntity<int>
    {
        #region == Field ==
        /// <summary>
        /// نام دسترسی
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// کنترلر دسترس
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// اکشن دسترس
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// نوع عملیات
        /// </summary>
        public PermissionActionType ActionType { get; set; }

        /// <summary>
        /// کلید گروه دسترسی
        /// </summary>
        public int PermissionGroupID { get; set; }
        #endregion

        #region == Navigation ==
        /// <summary>
        /// جدول گروه دسترسی
        /// </summary>
        public PermissionGroup PermissionGroup { get; set; }

        /// <summary>
        /// جدول دسترسی های نقش
        /// </summary>
        public ICollection<RolePermission> RolePermissions { get; set; }
        #endregion
    }
}
