﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace MadPay.Domain.Entities.Security
{
    /// <summary>
    /// نقش
    /// </summary>
    public class Role : IdentityRole<long>, IRegister<long>, IModifier<long>, IActive, IEntity
    {
        #region == Ctor ==
        public Role()
        {
            RolePermissions = new HashSet<RolePermission>();
            UserRoles = new HashSet<UserRole>();
        }
        #endregion

        #region == Fields ==
        /// <summary>
        /// نام جهت نمایش
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// وضعیت
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// ثبت کننده
        /// </summary>
        public long RegistererID { get; set; }

        /// <summary>
        /// تاریخ ثبت
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// ویرایش کننده
        /// </summary>
        public long? ModifierID { get; set; }

        /// <summary>
        /// تاریخ ویرایش
        /// </summary>
        public DateTime? ModifyDate { get; set; }
        #endregion

        #region == Navigation ==

        /// <summary>
        /// جدول کاربر ثبت کننده
        /// </summary>
        public User Registerer { get; set; }

        /// <summary>
        /// جدول کاربر ویرایش کننده
        /// </summary>
        public User Modifier { get; set; }

        /// <summary>
        /// جدول دسترسی های نقش
        /// </summary>
        public ICollection<RolePermission> RolePermissions { get; set; }

        /// <summary>
        /// لیست دسترسی های کاربر
        /// </summary>
        public ICollection<UserRole> UserRoles { get; set; }
        #endregion
    }
}
