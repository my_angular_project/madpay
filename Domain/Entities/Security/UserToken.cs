﻿using Microsoft.AspNetCore.Identity;

namespace MadPay.Domain.Entities.Security
{
    /// <summary>
    /// توکن های هر  کاربر
    /// </summary>
    public class UserToken : IdentityUserToken<long>, IEntity
    {
    }
}
