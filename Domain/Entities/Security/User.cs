﻿using MadPay.Domain.Entities.UserDetail;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace MadPay.Domain.Entities.Security
{
    /// <summary>
    /// کاربر
    /// </summary>
    public class User : IdentityUser<long>/*, IRegister<long>*/, IModifier<long>, IActive, IEntity
    {
        #region == Ctor ==
        public User()
        {
            UserPhotos = new HashSet<UserPhoto>();
            UserBankCards = new HashSet<UserBankCard>();
            UserRoles = new HashSet<UserRole>();
        }
        #endregion
        
        #region == Fields ==
        /// <summary>
        /// نام
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// نام خانوادگی
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// جنسیت
        /// </summary>
        public bool Gender { get; set; }

        /// <summary>
        /// شهر
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// تاریخ تولد
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// وضعیت
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// ثبت کننده
        /// </summary>
        public long? RegistererID { get; set; }

        /// <summary>
        /// تاریخ ثبت
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// ویرایش کننده
        /// </summary>
        public long? ModifierID { get; set; }

        /// <summary>
        /// تاریخ ویرایش
        /// </summary>
        public DateTime? ModifyDate { get; set; }
        #endregion

        #region  == Navigation ==
        /// <summary>
        /// لیست عکس کاربر
        /// </summary>
        public ICollection<UserPhoto> UserPhotos { get; set; }

        /// <summary>
        /// لیست کارت بانکی کاربر
        /// </summary>
        public ICollection<UserBankCard> UserBankCards { get; set; }

        /// <summary>
        /// لیست دسترسی های کاربر
        /// </summary>
        public ICollection<UserRole> UserRoles { get; set; }
        #endregion

        #region == Registerer && Modifier Navigation
        public ICollection<Role> RoleRegisterer { get; set; }
        public ICollection<Role> RoleModifier { get; set; }
        #endregion
    }
}
