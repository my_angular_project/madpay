﻿using System.Collections.Generic;

namespace MadPay.Domain.Entities.Geo
{
    public class Countries : BaseEntity<int>
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public ICollection<Province> Provinces { get; set; }
    }

    public class Province : BaseEntity<int>
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public int CountryId { get; set; }
        public Countries Country { get; set; }
        //public ICollection<City> Cities { get; set; }
    }

    //public class City : BaseEntity<int>
    //{
    //    public string Name { get; set; }
    //    public string Code { get; set; }
    //    public int ProvinceId { get; set; }
    //    public Province Province { get; set; }
    //}
}
