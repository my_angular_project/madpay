﻿using System;
using System.Collections.Generic;

namespace MadPay.Domain.Exceptions
{
    public class AppException : Exception
    {
        public AppException(string errorMessage) : base(errorMessage) { }
        public AppException(List<string> errorMessages) : base(string.Join(",", errorMessages)) { }
        public AppException(string errorMessage, Exception ex) : base(errorMessage, ex) { }
        public AppException(List<string> errorMessages, Exception ex) : base(string.Join(",", errorMessages), ex) { }
    }
}
