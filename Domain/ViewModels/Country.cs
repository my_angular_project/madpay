﻿using MadPay.Domain.Entities.Geo;
using System.Collections.Generic;

namespace MadPay.Domain.Dtos
{
    [DtoFor(typeof(Countries))]
    public class CountryVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public ICollection<ProvinceVM> Provinces { get; set; }
    }

    [DtoFor(typeof(Province))]
    public class ProvinceVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int CountryId { get; set; }
        public CountryVM Country { get; set; }
        //public ICollection<CityVM> Cities { get; set; }
    }

    public class CityVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int ProvinceId { get; set; }
        public ProvinceVM Province { get; set; }
    }
}
