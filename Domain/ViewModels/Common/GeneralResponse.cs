﻿using System.Collections.Generic;

namespace MadPay.Domain.ViewModels.Common
{
    public class GeneralResponse<T>
    {
        public bool Status { get; set; } = true;
        public IEnumerable<string> Messages { get; set; }
        public T Result { get; set; }
    }

    public class GeneralResponse
    {
        public bool Status { get; set; } = true;
        public IEnumerable<string> Messages { get; set; }
        public object Result { get; set; }
    }
}
