﻿using MadPay.Domain.Entities.Security;

namespace MadPay.Domain.ViewModels.Security.UserVM
{
    [DtoFor(typeof(User))]
    public class UserLoginVM
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public bool IsRemeberMe { get; set; }
    }
}
