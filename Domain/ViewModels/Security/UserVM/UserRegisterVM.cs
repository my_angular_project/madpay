﻿using MadPay.Domain.Entities.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace MadPay.Domain.ViewModels.Security.UserVM
{
    [DtoFor(typeof(User))]
    public class UserRegisterVM
    {
        public long Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public string PhoneNumber { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        public bool TwoFactorEnabled { get; set; }

        public bool LockoutEnabled { get; set; }

        public int AccessFailedCount { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool Gender { get; set; }

        public string City { get; set; }

        public bool IsActive { get; set; }
    }
}
