﻿using System.Collections.Generic;

namespace MadPay.Domain.ViewModels.Security.IdentityServerVM
{
    public class IdentityServerSetting
    {
        public string IdentityServerAuthority { get; set; }
        public IEnumerable<string> IdentityResources { get; set; }
        public IEnumerable<IdentityServerAPI> ApiResources { get; set; }
        public IEnumerable<IdentityServerClient> Client { get; set; }
    }

    public class IdentityServerAPI
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public IEnumerable<string> UserClaims { get; set; }
    }

    public class IdentityServerClient
    {
        public int AccessTokenLifeTime { get; set; }
        public string AllowedGrantTypes { get; set; }
        public string ClientID { get; set; }
        public bool AlwaysIncludeUserClaimsInIdToken { get; set; }
        public bool AlwaysSendClientClaims { get; set; }
        public IEnumerable<string> AllowCorsOrigins { get; set; }
        public bool RequireClientSecret { get; set; }
        public IEnumerable<string> AllowedScopes { get; set; }
        public bool AllowOfflineAccess { get; set; }

    }
}
