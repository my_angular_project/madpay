﻿using MadPay.Domain.Enums.Security;

namespace MadPay.Domain.ViewModels.Security.PermissionVM
{
    public class PermissionVM
    {
        public string Name { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public PermissionActionType ActionType { get; set; }

        public int PermissionGroupID { get; set; }
    }
}
