﻿using MadPay.Domain.Entities.Security;
using System.Collections.Generic;

namespace MadPay.Domain.ViewModels.Security.PermissionVM
{
    [DtoFor(typeof(PermissionGroup))]
    public class PermissionGroupVM
    {
        public int? ParentID { get; set; }

        public string Name { get; set; }

        public string ControllerName { get; set; }

        public List<PermissionVM> Permissions { get; set; }
    }
}
