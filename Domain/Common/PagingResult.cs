﻿using System.Collections.Generic;

namespace MadPay.Domain.Common
{
    public class PagingResult<T>
    {
        public IEnumerable<T> Data { get; set; }
        public long Count { get; set; }
    }
}
