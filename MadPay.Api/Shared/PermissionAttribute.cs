﻿using MadPay.Domain.Enums.Security;
using Microsoft.AspNetCore.Authorization;
using System;

namespace MadPay.Api
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class PermissionAttribute : AuthorizeAttribute
    {
        public string Name { get; }
        public PermissionActionType ActionType { get; }

        public PermissionAttribute(string name, PermissionActionType actionType) : base("Permission")
        {
            this.Name = name;
            this.ActionType = actionType;
        }
    }
}
