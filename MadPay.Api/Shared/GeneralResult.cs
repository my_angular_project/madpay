﻿using MadPay.Api.Shared;
using MadPay.Domain.ViewModels.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace MadPay.Api
{
    public sealed class GeneralResult : IConvertToActionResult
    {
        public ActionResult Result { get; }

        public GeneralResult(ActionResult result)
        {
            Result = result;
        }

        public static implicit operator GeneralResult(ActionResult result)
        {
            if (result is NOkObjectResult)
            {
                var xGeneralResult = new GeneralResponse
                {
                    Messages = ((NOkObjectResult)result).xErrorMessages,
                    Result = ((OkObjectResult)result)?.Value,
                    Status = ((NOkObjectResult)result).xErrorMessages == null || !((NOkObjectResult)result).xErrorMessages.Any()
                };

                return new GeneralResult(new OkObjectResult(xGeneralResult));
            }
            else if (result is OkObjectResult)
            {
                var xGeneralResult = new GeneralResponse
                {
                    Messages = new List<string>(),
                    Result = ((OkObjectResult)result)?.Value,
                    Status = true
                };

                return new GeneralResult(new OkObjectResult(xGeneralResult));
            }
            else if (result is BadRequestResult)
            {
                //TODO: Handle Error!
                return new GeneralResult(result); // Temprorary
            }
            else if (result is BadRequestObjectResult)
            {
                //TODO: Handle Error!
                return new GeneralResult(result); // Temprorary
            }
            else if (result is CreatedAtActionResult)
            {
                var xGeneralResult = new GeneralResponse
                {
                    Messages = new List<string>(),
                    Result = ((CreatedAtActionResult)result)?.Value,
                    Status = true
                };

                return new GeneralResult(new OkObjectResult(xGeneralResult));
            }
            else if (result is NotFoundResult)
            {
                //TODO: Handle Error!
                return new GeneralResult(result); // Temprorary
            }
            else
            {
                return new GeneralResult(result);
            }
        }

        IActionResult IConvertToActionResult.Convert()
        {
            return Result ?? new StatusCodeResult(500);
        }
    }
}
