import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  title = 'ClientApp';
  constructor(private http: HttpClient) {

  }

  ngOnInit(): void {
  }

  // loadData() {
  //   this.http.get('http://localhost:58941/api/WeatherForecast').subscribe(
  //     res => {
  //       this.result = res;
  //     }, err => console.log(err)
  //   );
  // }

}
