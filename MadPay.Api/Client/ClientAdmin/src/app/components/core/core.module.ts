import { NgModule } from '@angular/core';
import { FooterComponent } from './layout/footer/footer.component';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './layout/header/header.component';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { NotificationSidebarComponent } from './layout/notification-sidebar/notification-sidebar.component';
import { LayoutRoutingModule } from './layout/layout-routing.module';


@NgModule({
  declarations: [
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    NavbarComponent,
    NotificationSidebarComponent
  ],
  imports: [
    CommonModule,
    LayoutRoutingModule
  ],
  providers: [],
  exports: [
  ]
})
export class CoreModule { }
