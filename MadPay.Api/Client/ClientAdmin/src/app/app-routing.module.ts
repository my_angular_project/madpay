import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './components/core/layout/layout.component';

const appRoutes: Routes = [
  { path: '', component: LayoutComponent, pathMatch: 'full' },
  { path: 'auth', loadChildren: './components/auth/auth.module#AuthModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
