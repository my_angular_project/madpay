﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using MadPay.Api.Shared;
using MadPay.Domain.ViewModels.Security.PermissionVM;
using MadPay.Services.Service.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace MadPay.Api.Controllers.Security
{
    public class PermissionController : ApiControllerBase
    {
        #region == Ctor ==
        private readonly IActionDescriptorCollectionProvider _actionDescriptor;
        private readonly IPermissionService _permissionService;

        public PermissionController(IPermissionService permissionService, IActionDescriptorCollectionProvider actionDescriptor)
        {
            _actionDescriptor = actionDescriptor;
            _permissionService = permissionService;
        }
        #endregion

        #region == Read Method ==
        [HttpGet("UpdatePermissions")]
        //[Permission("دریافت تمام دسترسی های سیستم", ActionType.Read)]
        public async Task<GeneralResult> UpdatePermissions()
        {
            var totalList = _actionDescriptor.ActionDescriptors.Items.OfType<ControllerActionDescriptor>().Where(w => w.MethodInfo.CustomAttributes.Any(a => a.AttributeType == typeof(PermissionAttribute))).ToList();
            var permissionGroups = totalList.Select(z => z.ControllerName).Distinct().Select(controllerName => new PermissionGroupVM
            {
                Name = totalList.Where(w => w.ControllerName == controllerName).FirstOrDefault().ControllerTypeInfo.CustomAttributes.Any() ?
                       totalList.Where(w => w.ControllerName == controllerName).FirstOrDefault().ControllerTypeInfo.GetCustomAttribute<DescriptionAttribute>().Description :"بی نام",
                ControllerName = controllerName.Replace("Controller", ""),
                Permissions = totalList.Where(w => w.ControllerName == controllerName).Select(action => new PermissionVM
                {
                    Action = action.ActionName,
                    ActionType = action.MethodInfo.GetCustomAttribute<PermissionAttribute>().ActionType,
                    Controller = controllerName,
                    Name = action.MethodInfo.GetCustomAttribute<PermissionAttribute>().Name
                }).ToList()
            }).ToList();
            return Ok(await _permissionService.UpdateList(permissionGroups));
        }


        [HttpPost]
        public async Task<GeneralResult> Post()
        {
            return Ok(await _permissionService.AddRole());
        }
        #endregion

        #region == Prepare Method ==

        #endregion

        #region == Post Method ==

        #endregion

        #region == Put Method ==

        #endregion

        #region == Delete Method ==

        #endregion
    }
}
