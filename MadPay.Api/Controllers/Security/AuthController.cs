﻿using MadPay.Api.Shared;
using MadPay.Domain.ViewModels.Security.UserVM;
using MadPay.Services.Service.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MadPay.Api.Controllers.Security.User
{
    public class AuthController : ApiControllerBase
    {
        #region == Ctor ==
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            this._authService = authService;
        }
        #endregion

        #region == Read Method ==

        #endregion

        #region == Prepare Method ==

        #endregion

        #region == Post Method ==
        [HttpPost("Register")]
        [Authorize]
        public async Task<GeneralResult> Post(UserRegisterVM userRegister)
        {
            return Ok(await _authService.Register(userRegister));
        }
        #endregion

        #region == Put Method ==

        #endregion

        #region == Delete Method ==

        #endregion
    }
}
