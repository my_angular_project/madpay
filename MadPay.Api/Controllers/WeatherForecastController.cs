﻿using MadPay.Api.Shared;
using MadPay.Domain.Enums.Security;
using MadPay.Services.Service.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.Threading.Tasks;

namespace MadPay.Api.Controllers
{
    [Description("آب و هوا")]
    public class WeatherForecastController : ApiControllerBase
    {
        private readonly ICountryService _countryService;

        public WeatherForecastController(ICountryService countryService)
        {
            this._countryService = countryService;
        }

        [HttpGet]
        [Permission("دریافت آب و هوا", PermissionActionType.Read)]
        public async Task<IActionResult> Get()
        {
            var result = await _countryService.Get();
            return Ok(result);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post()
        {
            var a = User.Identity.IsAuthenticated;
            return Ok("Post Test");
        }
    }
}
