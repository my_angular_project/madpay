using IdentityServer4.Services;
using MadPay.Api.Middlewares;
using MadPay.Api.Shared;
using MadPay.Domain.Entities.Security;
using MadPay.Domain.Mapper;
using MadPay.Domain.ViewModels.Common;
using MadPay.Domain.ViewModels.Security.IdentityServerVM;
using MadPay.Persistence.Context;
using MadPay.Persistence.Repositories;
using MadPay.Services.Interface.Repository;
using MadPay.Services.Interface.Service;
using MadPay.Services.Service.Security.Identity;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;

namespace MadPay.Api
{
    public class Startup
    {
        #region == Ctor ==
        public IConfiguration _configuration { get; }
        public readonly JwtConfig _jwtConfig;
        private readonly IdentityServerSetting _identityServerSetting;
        private readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        private readonly IHostEnvironment Environment;


        public Startup(IConfiguration configuration, IHostEnvironment environment)
        {
            _configuration = configuration;
            _jwtConfig = _configuration.GetSection(nameof(JwtConfig)).Get<JwtConfig>();
            _identityServerSetting = configuration.GetSection(nameof(IdentityServerSetting)).Get<IdentityServerSetting>();
            Environment = environment;

        }
        #endregion

        #region == Configuration ==
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<JwtConfig>(_configuration.GetSection(nameof(JwtConfig)));
            services.Configure<IdentityServerSetting>(_configuration.GetSection(nameof(IdentityServerSetting)));

            services.AddScoped<IUnitOfWork, UnitOfWork<ApplicationDBContext>>();

            services.AddMapperConfigurations();
            services.AddServices();

            services.AddDbContext<ApplicationDBContext>(opt =>
            {
                opt.UseSqlServer(_configuration.GetConnectionString("ApplicationConnection"));
            });

            services.AddMvcCore(opt => opt.EnableEndpointRouting = false)
             .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
             .AddAuthorization()
             .AddNewtonsoftJson(options =>
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddResponseCaching();
            services.AddIdentityServerConfig(_identityServerSetting);
            services.AddApiAuthorization();

            services.AddCors();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            IdentityModelEventSource.ShowPII = true;
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseHsts();

            app.UseCors(i => i.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.AddExceptionHandling();
            app.UseResponseCaching();
            app.UseIdentityServer();
            //app.UseHttpContext();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "api/{controller}/{action}/{id?}");
            });
        }
        #endregion
    }
}
