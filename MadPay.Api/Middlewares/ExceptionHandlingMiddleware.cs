﻿using MadPay.Domain.Exceptions;
using MadPay.Domain.ViewModels.Common;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace MadPay.Api.Middlewares
{
    public class ExceptionHandlingMiddleware
    {
        #region == Ctor ==
        private readonly RequestDelegate _next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        #endregion

        #region == Detail Method ==
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var result = new GeneralResponse { Result = null, Status = false };
            var code = HttpStatusCode.InternalServerError;

            if (ex is AppException)
            {
                string errorText = $"کاربر گرامی {ex.Message}.";
                result.Messages = new List<string>() { errorText };
                code = HttpStatusCode.OK;
            }
            else
            {
                result.Messages = new List<string>() { ex.Message };
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(JsonConvert.SerializeObject(result));
        } 
        #endregion
    }
}
