﻿using Microsoft.AspNetCore.Builder;

namespace MadPay.Api.Middlewares
{
    public static class MiddlewareExtension
    {
        public static void AddExceptionHandling(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandlingMiddleware>();
        }
    }
}
