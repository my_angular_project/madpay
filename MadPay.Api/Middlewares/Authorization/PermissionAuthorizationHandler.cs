﻿using MadPay.Services.Service.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace MadPay.Api.Middlewares.Authorization
{
    public class PermissionAuthorizationHandler : AuthorizationHandler<PermissionRequirement>
    {
        #region == Ctor ==
        private readonly IUserService _userService;

        public PermissionAuthorizationHandler(IUserService userService)
        {
            _userService = userService;
        }
        #endregion

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            if (context.Resource is AuthorizationFilterContext)
            {
                if (_userService.IsLogin())
                {
                    var filterContext = ((AuthorizationFilterContext)context.Resource);
                    string action = filterContext.RouteData.Values["action"].ToString();
                    string controller = filterContext.RouteData.Values["controller"].ToString();
                    var currentUserID = _userService.GetCurrentUserID();

                    if (_userService.CheckPermission(controller, action, currentUserID))
                        context.Succeed(requirement);
                    else
                        context.Fail();
                }
                else
                    context.Fail();
            }
            return Task.CompletedTask;
        }
    }
}
