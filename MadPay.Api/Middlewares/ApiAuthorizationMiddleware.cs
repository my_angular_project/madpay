﻿using IdentityServer4.AccessTokenValidation;
using MadPay.Api.Middlewares.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace MadPay.Api.Middlewares
{
    public static class ApiAuthorizationMiddleware
    {
        public static void AddApiAuthorization(this IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(opt =>
                 {
                     opt.Authority = "https://localhost:5000";
                     opt.RequireHttpsMetadata = true;
                     opt.Audience = "MadPay";
                     opt.TokenValidationParameters.ValidateAudience = false;
                     opt.TokenValidationParameters.ValidateIssuer = false;
                     opt.TokenValidationParameters.ValidateIssuerSigningKey = false;
                     opt.TokenValidationParameters.ValidateLifetime = false;
                     opt.TokenValidationParameters.ValidateTokenReplay = false;
                     opt.IncludeErrorDetails = true;
                     //opt.TokenValidationParameters = new TokenValidationParameters
                     //{
                     //    ValidateAudience = false
                     //};
                 });

            //services.AddAuthentication(defaultScheme: IdentityServerAuthenticationDefaults.AuthenticationScheme)
            //   .AddIdentityServerAuthentication(options =>
            //   {
            //       options.Authority = "https://localhost:5000";
            //       options.ApiName = "MadPay";
            //       options.RequireHttpsMetadata = true;
            //   });

            services.AddScoped<IAuthorizationHandler, PermissionAuthorizationHandler>();

            services.AddAuthorization(option =>
                option.AddPolicy("Permission", builder =>
                    builder.AddRequirements(new PermissionRequirement()).RequireAuthenticatedUser()
                )
            );
        }
    }
}
