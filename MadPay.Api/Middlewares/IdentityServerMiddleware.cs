﻿using IdentityServer4.Models;
using MadPay.Domain.Entities.Security;
using MadPay.Domain.ViewModels.Security.IdentityServerVM;
using MadPay.Persistence.Context;
using MadPay.Services.Service.Security.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;

namespace MadPay.Api.Middlewares
{
    public static class IdentityServerMiddleware
    {
        public static void AddIdentityServerConfig(this IServiceCollection services, IdentityServerSetting config)
        {
            var finalConfig = MapJsonToConfig(config);

            services.AddIdentity<User, Role>(opt =>
            {
                opt.Password.RequireLowercase = false;
                opt.Password.RequireUppercase = false;
                opt.Password.RequireNonAlphanumeric = false;

                opt.User.RequireUniqueEmail = true;

                opt.SignIn.RequireConfirmedAccount = true;
                opt.SignIn.RequireConfirmedEmail = true;
            })
            .AddEntityFrameworkStores<ApplicationDBContext>()
            .AddUserManager<AppUserManager>()
            //.AddSignInManager<AppSignInManager>()
            .AddErrorDescriber<AppErrorDescriberService>()
            .AddDefaultTokenProviders();

            services.AddIdentityServer(options =>
            {
                options.Events.RaiseErrorEvents = true;
                options.Events.RaiseInformationEvents = true;
                options.Events.RaiseFailureEvents = true;
                options.Events.RaiseSuccessEvents = true;
                
            })

                    .AddDeveloperSigningCredential()
                    .AddInMemoryIdentityResources(finalConfig.IdentityResources)
                    .AddInMemoryApiResources(finalConfig.Apis)
                    .AddInMemoryClients(finalConfig.Clients)
                    .AddAspNetIdentity<User>()
                    .AddResourceOwnerValidator<AppIdentityPasswordValidator<User>>();
                    //.AddProfileService<AppClaimsProfileService>();
        }

        #region == Private Method ==
        private static IdentityServerInMemomryConfig MapJsonToConfig(IdentityServerSetting setting)
        {
            var finalConfig = new IdentityServerInMemomryConfig();

            #region == Resources ==
            if (setting.IdentityResources != null && setting.IdentityResources.Any())
            {
                finalConfig.IdentityResources = setting.IdentityResources.Select(s =>
                {
                    switch (s.ToLower())
                    {
                        case "openid":
                            return new IdentityResources.OpenId();
                        default:
                            return null;
                    }
                }).ToArray();
            }
            #endregion

            #region == Apis ==
            if (setting.ApiResources != null && setting.ApiResources.Any())
                finalConfig.Apis = setting.ApiResources.Select(s => new ApiResource(s.Name, s.DisplayName)
                {
                    UserClaims = s.UserClaims.Select(u => u).ToList()
                }).ToList();
            #endregion

            #region == Clients ==
            if (setting.Client != null && setting.Client.Any())
            {
                finalConfig.Clients = setting.Client.Select(s => new Client
                {
                    AccessTokenLifetime = s.AccessTokenLifeTime,
                    AllowedGrantTypes = GetGrantTypeFromString(s.AllowedGrantTypes),
                    ClientId = s.ClientID,
                    AlwaysIncludeUserClaimsInIdToken = s.AlwaysIncludeUserClaimsInIdToken,
                    AlwaysSendClientClaims = s.AlwaysSendClientClaims,
                    AllowedCorsOrigins = s.AllowCorsOrigins.ToList(),
                    RequireClientSecret = s.RequireClientSecret,
                    AllowedScopes = s.AllowedScopes.ToList(),
                    AllowOfflineAccess = s.AllowOfflineAccess,
                    AbsoluteRefreshTokenLifetime = 2592000
                }).ToArray();
            }
            #endregion

            return finalConfig;
        }

        private static ICollection<string> GetGrantTypeFromString(string GrantType)
        {
            switch (GrantType.ToLower())
            {
                case "password":
                    return GrantTypes.ResourceOwnerPassword;
                case "implicit":
                    return GrantTypes.Implicit;
                default:
                    return null;
            }
        }
        #endregion
    }

    public class IdentityServerInMemomryConfig
    {
        public IdentityResource[] IdentityResources { get; set; }
        public List<ApiResource> Apis { get; set; }
        public Client[] Clients { get; set; }
    }
}
