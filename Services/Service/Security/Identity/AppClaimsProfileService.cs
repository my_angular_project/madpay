﻿using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using MadPay.Domain.Entities.Security;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MadPay.Services.Service.Security.Identity
{
    public class AppClaimsProfileService : IProfileService
    {
        #region == Ctor ==
        private readonly AppUserManager _userManager;
        private readonly IUserClaimsPrincipalFactory<User> _claimsFactory;

        public AppClaimsProfileService(AppUserManager userManager, IUserClaimsPrincipalFactory<User> claimsFactory)
        {
            _userManager = userManager;
            _claimsFactory = claimsFactory;
        }
        #endregion

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            string sub = context.Subject.GetSubjectId();
            var user = await _userManager.FindByIdAsync(sub);
            var claims = new List<Claim>();
            
            claims.Add(new Claim(JwtRegisteredClaimNames.NameId, user.Id.ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName));
            claims.Add(new Claim(JwtRegisteredClaimNames.GivenName, user.FirstName));
            claims.Add(new Claim(JwtRegisteredClaimNames.FamilyName, user.LastName));
            claims.Add(new Claim(JwtRegisteredClaimNames.Email, user.Email));

            context.IssuedClaims = claims;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await _userManager.FindByIdAsync(sub);
            context.IsActive = user != null;
        }
    }
}
