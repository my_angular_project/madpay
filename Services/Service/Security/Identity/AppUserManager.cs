﻿using MadPay.Domain.Entities.Security;
using MadPay.Domain.ViewModels.Security.UserVM;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace MadPay.Services.Service.Security.Identity
{
    public class AppUserManager : UserManager<User>
    {
        #region == Ctor == 
        private readonly IConfiguration _configuration;


        public AppUserManager(
          IUserStore<User> store,
          IOptions<IdentityOptions> optionsAccessor,
          IPasswordHasher<User> passwordHasher,
          IEnumerable<IUserValidator<User>> userValidators,
          IEnumerable<IPasswordValidator<User>> passwordValidators,
          ILookupNormalizer keyNormalizer,
          IdentityErrorDescriber errors,
          IServiceProvider services,
          ILogger<UserManager<User>> logger,
          IConfiguration configuration
            ) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            this._configuration = configuration;
        }
        #endregion

        /// <summary>
        /// Get Token
        /// </summary>
        /// <param name="user">Instance OF User</param>
        /// <returns>Token</returns>
        public string GenerateToken(User user)
        {
            string issuer = _configuration["JwtConfig:Issuer"];
            string audience = _configuration["JwtConfig:Audience"];
            string key = _configuration["JwtConfig:Key"];
            DateTime expireDate = DateTime.Now.AddHours(6);

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.NameId,user.Id.ToString(),ClaimValueTypes.Integer64),
                new Claim(JwtRegisteredClaimNames.UniqueName,user.UserName),
                new Claim(JwtRegisteredClaimNames.GivenName,user.FirstName),
                new Claim(JwtRegisteredClaimNames.FamilyName,user.LastName),
                new Claim(JwtRegisteredClaimNames.Sub,user.Id.ToString()),
            };

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var tokenOption = new JwtSecurityToken(issuer, audience, claims, null, expireDate, credentials);
            var tokenHandler = new JwtSecurityTokenHandler();
            return tokenHandler.WriteToken(tokenOption);
        }
    }
}
