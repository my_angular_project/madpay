﻿using AutoMapper;
using MadPay.Domain.Entities.Security;
using MadPay.Domain.ViewModels.Security.PermissionVM;
using MadPay.Services.Interface.Repository;
using MadPay.Services.Service.Base;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualBasic;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MadPay.Services.Service.Security
{
    #region == Interface ==
    public interface IPermissionService
    {
        Task<bool> UpdateList(List<PermissionGroupVM> xPermissionGroups);

        Task<bool> AddRole();
    }
    #endregion

    #region == Implement ==
    public class PermissionService : BaseService, IPermissionService
    {
        //private readonly RoleManager
        private readonly RoleManager<Role> _roleManager;

        #region == Ctor ==
        public PermissionService(IUnitOfWork uow, IMapper mapper, RoleManager<Role> roleManager) : base(uow, mapper)
        {
            _roleManager = roleManager;
        }

        public async Task<bool> AddRole()
        {
            await _roleManager.CreateAsync(new Role()
            {
                Name = "Admin",
                DisplayName = "Admin",
                IsActive = true,
                RegistererID = 1,
                RegisterDate = DateAndTime.Now
            });

            await uow.SaveChangesAsync();
            return true;
        }
        #endregion

        #region == Read Method ==

        #endregion

        #region == Prepare Method ==

        #endregion

        #region == Post Method ==

        #endregion

        #region == Put Method ==
        public async Task<bool> UpdateList(List<PermissionGroupVM> permissionGroupList)
        {
            var dbPermissions = await uow.PermissionRepository.GetAsync();
            var dbPermissionGroups = await uow.PermissionGroupRepository.GetAsync();

            foreach (var group in permissionGroupList)
            {
                var xPermissionGroup = dbPermissionGroups.FirstOrDefault(a => a.Controller == group.ControllerName);

                if (xPermissionGroup == null) // New Group 
                {
                    var newGroupDB = new PermissionGroup { Name = group.Name, Controller = group.ControllerName };
                    uow.PermissionGroupRepository.Insert(newGroupDB);
                    await uow.SaveChangesAsync();

                    foreach (var item in group.Permissions)
                    {
                        var xPermission = dbPermissions.FirstOrDefault(a => a.Action == item.Action && a.Controller == group.ControllerName);

                        if (xPermission == null)
                            uow.PermissionRepository.Insert(new Permission
                            {
                                PermissionGroupID = newGroupDB.Id,
                                Controller = group.ControllerName,
                                Action = item.Action,
                                Name = item.Name,
                                ActionType = item.ActionType,
                            });
                        else
                        {
                            if (xPermission.Name != item.Name)
                                xPermission.Name = item.Name;

                            if (xPermission.ActionType != item.ActionType)
                                xPermission.ActionType = item.ActionType;

                            if (xPermission.PermissionGroupID != newGroupDB.Id)
                                xPermission.PermissionGroupID = newGroupDB.Id;

                            uow.PermissionRepository.Update(xPermission);
                        }
                    }
                }
                else // Group Exist
                {
                    if (xPermissionGroup.Name != group.Name)
                    {
                        xPermissionGroup.Name = group.Name;
                        uow.PermissionGroupRepository.Update(xPermissionGroup);
                    }

                    foreach (var xItem in group.Permissions)
                    {
                        var xPermission = dbPermissions.FirstOrDefault(a => a.Action == xItem.Action && a.Controller == group.ControllerName);

                        if (xPermission == null)
                            uow.PermissionRepository.Insert(new Permission
                            {
                                PermissionGroupID = xPermissionGroup.Id,
                                Controller = group.ControllerName,
                                Action = xItem.Action,
                                Name = xItem.Name,
                                ActionType = xItem.ActionType
                            });
                        else
                        {
                            if (xPermission.Name != xItem.Name)
                                xPermission.Name = xItem.Name;

                            if (xPermission.ActionType != xItem.ActionType)
                                xPermission.ActionType = xItem.ActionType;

                            if (xPermission.PermissionGroupID != xPermissionGroup.Id)
                                xPermission.PermissionGroupID = xPermissionGroup.Id;

                            uow.PermissionRepository.Update(xPermission);
                        }
                    }
                }
                await uow.SaveChangesAsync();
            }

            //TODO Check This SaveChanges()
            await uow.SaveChangesAsync();
            return true;
        }
        #endregion

        #region == Delete Method ==

        #endregion

    }
    #endregion
}
