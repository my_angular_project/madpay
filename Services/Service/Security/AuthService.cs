﻿using AutoMapper;
using MadPay.Domain.Entities.Security;
using MadPay.Domain.Exceptions;
using MadPay.Domain.ViewModels.Security.UserVM;
using MadPay.Services.Interface.Repository;
using MadPay.Services.Service.Base;
using MadPay.Services.Service.Security.Identity;
using MadPay.Utilities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace MadPay.Services.Service.Security
{
    #region == Interface ==
    public interface IAuthService
    {
        Task<bool> Register(UserRegisterVM userRegister);
    }
    #endregion

    #region == Implement ==
    public class AuthService : BaseService, IAuthService
    {
        #region == Ctor ==
        private readonly AppUserManager _userManager;
        private readonly SignInManager<User> _signInManager;

        public AuthService(IUnitOfWork uow, IMapper mapper, AppUserManager userManager, SignInManager<User> signInManager) : base(uow, mapper)
        {
            this._userManager = userManager;
            this._signInManager = signInManager;
        }

        #endregion

        #region == Read Method ==

        #endregion

        #region == Prepare Method ==

        #endregion

        #region == Post Method ==
        public async Task<bool> Register(UserRegisterVM userRegister)
        {
            userRegister.PhoneNumberConfirmed = false;
            userRegister.EmailConfirmed = false;
            userRegister.AccessFailedCount = 0;
            userRegister.IsActive = true;

            var user = _mapper.Map<User>(userRegister);
            user.RegisterDate = DateTime.Now;


            var result = await _userManager.CreateAsync(user, userRegister.Password);

            if (result.Succeeded)
            {
                //TODO Send Email Or Sms Confirm
                //if (_userManager.Options.SignIn.RequireConfirmedEmail)
                //{
                //    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                //    var callBackUrl = Url.RouteUrl("ConfirmEmail", new { code, key = user.GeneratedKey },
                //        Request.Scheme);

                //    var message = $"<a href=\"{callBackUrl}\"> Confirm Email </a>";

                //    await _emailSender.SendEmailAsync(user.Email, "Confirm Email", message);
                //}

                return true;
            }

            throw new AppException(IdentityHelper.AddIdentityErrors(result));
        }
        #endregion

        #region == Put Method ==

        #endregion

        #region == Delete Method ==

        #endregion
    }
    #endregion
}
