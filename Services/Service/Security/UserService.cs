﻿using AutoMapper;
using MadPay.Domain.Common;
using MadPay.Domain.Dtos;
using MadPay.Domain.Entities.Geo;
using MadPay.Services.Interface.Repository;
using MadPay.Services.Service.Base;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MadPay.Services.Service.Security
{
    #region == Interface ==
    public interface IUserService
    {
        long GetCurrentUserID();
        bool CheckPermission(string xControllerName, string xActionName, long? xUserID = null);
        bool IsLogin();
    }
    #endregion

    #region == Implement ==
    public class UserService : BaseService, IUserService
    {
        private readonly IHttpContextAccessor _httpContext;

        #region == Ctor ==
        public UserService(IUnitOfWork uow, IMapper IMapper, IHttpContextAccessor httpContext) : base(uow, IMapper)
        {
            _httpContext = httpContext;
        }
        #endregion

        #region == Read Method ==
        public long GetCurrentUserID()
        {
            if (IsLogin())
            {
                long xResult;
                if (long.TryParse(_httpContext.HttpContext.User.FindFirstValue(JwtRegisteredClaimNames.NameId), out xResult))
                    return xResult;
                else
                    return 0;
            }
            else
                return 0;
        }

        public bool CheckPermission(string controller, string action, long? userId = null)
        {
            if (userId == null)
                userId = GetCurrentUserID();

            return uow.UserRepository.Any(w => w.Id == userId.Value && w.UserRoles.SelectMany(x => x.Role.RolePermissions)
                                     .Any(d => d.Permission.Controller == controller && d.Permission.Action == action));
        }
        #endregion

        #region == Prepare Method ==
        public bool IsLogin()
        {
            return _httpContext.HttpContext.User.Identity.IsAuthenticated;
        }
        #endregion

        #region == Post Method ==

        #endregion

        #region == Put Method ==

        #endregion

        #region == Delete Method ==

        #endregion
    }
    #endregion

    public interface ICountryService
    {
        Task<List<CountryVM>> Get();
    }



    public class CountryService : BaseService, ICountryService
    {
        #region == Ctor ==
        public CountryService(IUnitOfWork uow, IMapper IMapper) : base(uow, IMapper)
        {

        }


        public async Task<List<CountryVM>> Get()
        {
            //var xRole = uow.RoleRepository.Get(x => x.Id == xID, null, new MapBy<Role>(x => x.xPermissionRoles.And().xPermission)).FirstOrDefault();
            //uow.UserRepository.Get(x => x.Id == xID, null, new MapBy<User>(x => x.xUserRoles)
            var countries = (await uow.CountryRepository.GetAsync(null, null, new MapBy<Countries>(i => i.Provinces))).ToList();
            var result = _mapper.Map<List<CountryVM>>(countries);
            //throw new AppException("Error Throwed");
            return result;
        }
        #endregion
    }
}
