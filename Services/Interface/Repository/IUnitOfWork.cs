﻿using MadPay.Domain.Entities.Geo;
using MadPay.Domain.Entities.Security;
using System;
using System.Threading.Tasks;

namespace MadPay.Services.Interface.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        //TODO Delete Sample Repository
        IBaseRepository<Countries> CountryRepository { get; }

        #region == Security ==
        IBaseRepository<User> UserRepository { get; }
        IBaseRepository<Role> RoleRepository { get; }
        IBaseRepository<PermissionGroup> PermissionGroupRepository { get; }
        IBaseRepository<Permission> PermissionRepository { get; }
        #endregion

        #region == Base ==
        void SaveChanges();

        Task SaveChangesAsync();
        #endregion
    }
}
